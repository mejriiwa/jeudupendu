*#!/bin/bash

# main file

# Message de bienvenue
echo "Bienvenue au jeu du pendu ! Voici les règles du jeu :
    Le but de jeu du pendu est de deviner un mot en proposant des lettres.
    Vous allez pouvoir choisir le niveau de difficulté en définissant la langue, la longueur du mot, et si vous souhaitez faire apparaitre la première lettre ou non.
    Des tirets correspondants aux lettres du mot appairaitront ensuite à l'écran. Vous aurez la possibilité de proposer une lettre ou un mot.
    Si la lettre proposée est bonne, elle remplacera le tiret correspondant et vous pourrez continuez la partie. Si la lettre est fausse, le pendu se dessinera petit à petit.
    Si vous parvenez à trouver le bon mot vous aurez gagné la partie !"

# Affichage des difficultés disponibles et leurs descriptions
function difficulte {
    echo -e "Veuillez choisir une difficulté parmi les suivantes :\n-Facile (1) : Moins de 5 lettres\n-Moyen (2): Entre 5 et 10 lettres\n-Difficile (3): Plus de 10 lettres"

    # Demander à l'utilisateur de choisir la difficulté :
    read -p 'Tapez le numéro correspondant à la difficulté : ' choix_difficulte

    if [ "$choix_difficulte" = "1" ] ; then
        echo "Difficulté choisie : Facile"
    elif [ "$choix_difficulte" = "2" ] ; then
        echo "Difficulté choisie : Moyen"
    elif [ "$choix_difficulte" = "3" ] ; then
        echo "Difficulté choisie : Difficile"
    else
        echo "Veuillez saisir une difficulté parmi la liste"
        difficulte
    fi
}

difficulte

# Demander à l'utilisateur de choisir une langue entre Français, Anglais et Espagnol
function langage {
    read -p 'Veuillez choisir la langue du jeu en tapant le numéro correspondant : Français (1) Anglais (2) Espagnol (3) : ' choix_langue

    if [ "$choix_langue" = "1" ] ; then
        echo "Langue choisie : Français"
    elif [ "$choix_langue" = "2" ] ; then
        echo "Langue choisie : Anglais"
    elif [ "$choix_langue" = "3" ] ; then
        echo "Langue choisie : Espagnol"
    else
        echo "Veuillez saisir une langue parmi la liste"
        langage
    fi
}

langage

# Définir le nombre d'essais en fonction de la difficulté
if [ "$choix_difficulte" = "1" ]; then
    essai=10
elif [ "$choix_difficulte" = "2" ]; then
    essai=10
elif [ "$choix_difficulte" = "3" ]; then
    essai=10
fi

# Fonction qui génère aléatoirement un mot en fonction de la difficulté et de la langue
function generer_mot {
    if [ "$choix_langue" = "1" ] ; then
        dictionnaire="/usr/share/dict/french"
    elif [ "$choix_langue" = "2" ] ; then
        dictionnaire="/usr/share/dict/words"
    elif [ "$choix_langue" = "3" ] ; then
        dictionnaire="/usr/share/dict/spanish"
    fi

    if [ "$choix_difficulte" = "1" ] ; then
        filtre=$(grep -E '^.{1,4}$' "$dictionnaire")
    elif [ "$choix_difficulte" = "2" ] ; then
        filtre=$(grep -E '^.{5,10}$' "$dictionnaire")
    else
        filtre=$(grep -E '^.{11,}$' "$dictionnaire")
    fi

    mot_aleatoire=$(echo "$filtre" | shuf -n 1)
}

generer_mot

# Fonction affiche le mot sous forme de tiret avec la première lettre
function affiche_premiere_lettre {
    nbre_lettre=${#1}

    echo -n "${1:0:1}"
    for (( i=1; i<nbre_lettre; i++ )); do
        echo -n "-"
    done
    echo
}

# Fonction qui affiche le mot sous forme de tiret sans la première lettre
function affiche_tiret {
    nbre_lettre=${#1}

    for (( i=0; i<nbre_lettre; i++ )); do
        echo -n "-"
    done
    echo
}

# Choix première lettre ou non
read -p "Voulez-vous faire apparaître la première lettre ? oui ou non : " rep

if [ "$rep" = "oui" ]; then
    mot_jeu=$(affiche_premiere_lettre "$mot_aleatoire")
else
    mot_jeu=$(affiche_tiret "$mot_aleatoire")
fi

# Fonction affiche tirets (non utilisée dans le script)
function affiche_tirets {
    nbre_lettre=${#1}

    for (( i=1; i<nbre_lettre; i++ )); do
        echo -n "-"
    done
    echo
}

# Boucle de jeu : arrête si mot a été trouvé OU on a plus d'essais
while [ "$mot_aleatoire" != "$mot_jeu" ] && [ "$essai" -gt 0 ]; do

    # Affichage de l'évolution du pendu
    PENDU=( "  _______ " "  |     | " "  |     | " "  |     | " "  |     | " "  |     O " "  |    /|\ " "  |    / \ " "  |       " "__|__     " )
    nb_traits=${#PENDU[@]}

    function afficher_evolution_pendu {
        traits=$((nb_traits - $1))
        for trait in "${PENDU[@]:0:traits}" ; do
            echo "$trait"
        done
    }

    afficher_evolution_pendu "$essai"

    echo "$mot_jeu"

    read -p 'Veuillez choisir une lettre ou un mot à saisir : ' reponse

    # Fonction lettre absente
    function lettre_absente {
        if [[ ! "$mot_aleatoire" == *"$1"* ]]; then
            echo "Attention, la lettre n'est pas dans le mot."
            ((essai--))
        fi
    }

    # Fonction test mot
    function test_mot {
        saisie_utilisateur=$1
        longueur=${#saisie_utilisateur}
        if [ "$longueur" -gt 1 ]; then
            if [ "$saisie_utilisateur" = "$mot_aleatoire" ]; then
                mot_jeu="$mot_aleatoire"
            else
                ((essai-=2))
            fi
        else
            completer_mot_jeu "$saisie_utilisateur"
        fi
    }

    # Fonction compléter mot jeu
    function completer_mot_jeu {
        lettre=$1
        longueur_mot=${#mot_aleatoire}
        resultat=""
        for (( i=0; i<longueur_mot; i++ )); do
            if [ "${mot_aleatoire:$i:1}" = "$lettre" ]; then
                resultat+="$lettre"
            else
                resultat+="${mot_jeu:$i:1}"
            fi
        done
        mot_jeu="$resultat"
    }

    if [ "${#reponse}" -gt 1 ]; then
        test_mot "$reponse"
    else
        lettre_absente "$reponse"
        completer_mot_jeu "$reponse"
    fi

    # Fonction qui précise le nombre d'essais restants (quand il en reste moins de 3)
    if [ "$essai" -le 3 ]; then
        echo "Il vous reste $essai essais restants."
    fi

done

# Message game over
if [ "$essai" -le 0 ]; then
    echo "Game over"
    echo "$mot_aleatoire"
fi

# Message victoire
if [ "$mot_jeu" = "$mot_aleatoire" ]; then
    echo "Bravo, tu as gagné !!!"
fi

# Proposer à l'utilisateur de rejouer
read -p "Voulez-vous continuer le jeu ? oui ou non : " rep

if [ "$rep" = "oui" ]; then
    echo "Continue"
    ./main.sh
else
    echo "Exit"
    exit
fi

