echo "Bienvenue au jeu du pendu ! Voici les règles du jeu : 
	
	Le but de jeu du pendu est de deviner un mot en proposant des lettres.
	Vous allez pouvoir choisir le niveau de difficulté en définissant la langue, la longueur du mot, et si vous souhaitez faire apparaitre la première lettre ou non.
	Des tirets correspondants aux lettres du mot appairaitront ensuite à l'écran. Vous aurez la possibilité de proposer une lettre ou un mot.
	 Si la lettre proposée est bonne, elle remplacera le tiret correspondant et vous pourrez continuez la partie. Si la lettre est fausse, le pendu se dessinera petit à petit.
	Si vous parvenez à trouver le bon mot vous aurez gagnez la partie ! "

